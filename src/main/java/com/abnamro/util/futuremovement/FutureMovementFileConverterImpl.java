package com.abnamro.util.futuremovement;

import com.abnamro.util.futuremovement.model.InputFileField;
import com.abnamro.util.futuremovement.model.InputFileFieldDefinition;
import com.abnamro.util.futuremovement.model.InputSystem;
import javafx.util.Pair;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.abnamro.util.futuremovement.model.InputFileField.AccountNumber;
import static com.abnamro.util.futuremovement.model.InputFileField.ClientNumber;
import static com.abnamro.util.futuremovement.model.InputFileField.ClientType;
import static com.abnamro.util.futuremovement.model.InputFileField.ExchangeCode;
import static com.abnamro.util.futuremovement.model.InputFileField.ExpirationDate;
import static com.abnamro.util.futuremovement.model.InputFileField.ProductGroupCode;
import static com.abnamro.util.futuremovement.model.InputFileField.QuantityLong;
import static com.abnamro.util.futuremovement.model.InputFileField.QuantityShort;
import static com.abnamro.util.futuremovement.model.InputFileField.SubaccountNumber;
import static com.abnamro.util.futuremovement.model.InputFileField.Symbol;

/**
 * Implementation class of {@link FutureMovementFileConverter}
 */
public class FutureMovementFileConverterImpl implements FutureMovementFileConverter {

    private static final Logger log = LoggerFactory.getLogger(FutureMovementFileConverter.LOGGER_NAME);

    @Override
    public void convertFutureMovementFile(InputSystem inputSystem, File inputFile, File outputFile) throws IOException {
        log.info("Start processing.");

        Map<InputFileField, InputFileFieldDefinition> inputFileFieldDefinitionMap = getInputFileFieldDefinition(inputSystem);

        Map<Pair<String, String>, BigDecimal> clientProductAmountMap = new HashMap<>();
        log.info("Processing input file...");
        int i = 0;
        LineIterator lineIterator = FileUtils.lineIterator(inputFile);
        while (lineIterator.hasNext()) {
            i++;
            log.debug("Reading input line " + i);
            String inputLine = lineIterator.nextLine();
            String clientInfo = getClientInformationString(inputLine, inputFileFieldDefinitionMap);
            log.debug("Client info: " + clientInfo);
            String productInfo = getProductInformation(inputLine, inputFileFieldDefinitionMap);
            log.debug("Product info: " + productInfo);
            Pair<String, String> clientProductPair = new Pair<>(clientInfo, productInfo);
            BigDecimal transactionAmount = getTransactionAmount(inputLine, inputFileFieldDefinitionMap);
            log.debug("Transaction amount: " + transactionAmount.toString());
            addToTotalTransactionAmount(clientProductAmountMap, clientProductPair, transactionAmount);
        }
        lineIterator.close();
        log.info("Finished processing input file. Number of lines read: " + i + ". Number of unique client-product pair: " + clientProductAmountMap.size());

        log.info("Writing output CSV report...");
        List<String> outputLines = new ArrayList<>(clientProductAmountMap.size() + 1);
        // write header line
        String headerLine = getHeaderLine();
        outputLines.add(headerLine);
        // write data lines
        for (Pair<String, String> clientProductPair : clientProductAmountMap.keySet()) {
            String outputLine = getOutputLine(clientProductPair, clientProductAmountMap);
            outputLines.add(outputLine);
        }
        FileUtils.writeLines(outputFile, outputLines);
        log.info("Finished writing output CSV report. File path: " + outputFile.getAbsolutePath());
        log.info("Processing complete.");
    }

    protected Map<InputFileField, InputFileFieldDefinition> getInputFileFieldDefinition(InputSystem inputSystem) {
        log.info("Loading input file field definition for " + inputSystem.getDisplayName());
        InputFileFieldDefinitionLoader inputFileFieldDefinitionLoader = InputFileFieldDefinitionLoaderFactory.getInputFileFieldDefinitionLoader(inputSystem);
        return inputFileFieldDefinitionLoader.loadInputFileFieldDefinition();
    }

    protected String getHeaderLine() {
        return "Client_Information,Product_Information,Total_Transaction_Amount";
    }

    protected String getOutputLine(Pair<String, String> clientProductPair, Map<Pair<String, String>, BigDecimal> inputFileFieldDefinitionMap) {
        return clientProductPair.getKey() +
                "," +
                clientProductPair.getValue() +
                "," +
                inputFileFieldDefinitionMap.get(clientProductPair).toString();
    }

    protected String getClientInformationString(String inputLine, Map<InputFileField, InputFileFieldDefinition> inputFileFieldDefinitionMap) {
        return getFieldStringValue(inputLine, ClientType, inputFileFieldDefinitionMap) +
                getFieldStringValue(inputLine, ClientNumber, inputFileFieldDefinitionMap) +
                getFieldStringValue(inputLine, AccountNumber, inputFileFieldDefinitionMap) +
                getFieldStringValue(inputLine, SubaccountNumber, inputFileFieldDefinitionMap);
    }

    protected String getProductInformation(String inputLine, Map<InputFileField, InputFileFieldDefinition> inputFileFieldDefinitionMap) {
        return getFieldStringValue(inputLine, ExchangeCode, inputFileFieldDefinitionMap) +
                getFieldStringValue(inputLine, ProductGroupCode, inputFileFieldDefinitionMap) +
                getFieldStringValue(inputLine, Symbol, inputFileFieldDefinitionMap) +
                getFieldStringValue(inputLine, ExpirationDate, inputFileFieldDefinitionMap);
    }

    protected BigDecimal getTransactionAmount(String inputLine, Map<InputFileField, InputFileFieldDefinition> inputFileFieldDefinitionMap) {
        // quantity long
        String quantityLongString = getFieldStringValue(inputLine, QuantityLong, inputFileFieldDefinitionMap);
        BigDecimal quantityLong = new BigDecimal(quantityLongString.trim());
        // quantity short
        String quantityShortString = getFieldStringValue(inputLine, QuantityShort, inputFileFieldDefinitionMap);
        BigDecimal quantityShort = new BigDecimal(quantityShortString.trim());

        return quantityLong.subtract(quantityShort);
    }

    protected String getFieldStringValue(String inputLine, InputFileField inputFileField, Map<InputFileField, InputFileFieldDefinition> inputFieldInputFileFieldDefinitionMap) {
        InputFileFieldDefinition inputFileFieldDefinition = inputFieldInputFileFieldDefinitionMap.get(inputFileField);
        int startPosition = inputFileFieldDefinition.getStartPosition();
        int length = inputFileFieldDefinition.getLength();
        return inputLine.substring(startPosition - 1, startPosition + length - 1);
    }

    /**
     * Adds the transaction amount to the total transaction amount.
     * If total transaction amount already exists for a client-product pair, calculate new total and store it in the <code>Map</code>.
     * Otherwise puts the transaction amount into the map as the initial total amount.
     *
     * @param clientProductAmountMap map of client-product and total transaction amount. The map will be updated.
     * @param clientProductPair      the client-product pair
     * @param transactionAmount      the transaction amount. This will be added to the existing total transaction amount of the client-product pair, or used as the initial total amount if new.
     */
    protected void addToTotalTransactionAmount(Map<Pair<String, String>, BigDecimal> clientProductAmountMap, Pair<String, String> clientProductPair, BigDecimal transactionAmount) {
        BigDecimal totalTransactionAmount = clientProductAmountMap.get(clientProductPair);
        if (totalTransactionAmount == null) {
            log.debug("New client-product pair: " + clientProductPair.getKey() + ", " + clientProductPair.getValue());
            totalTransactionAmount = transactionAmount;
        } else {
            log.debug("Updating existing pair: " + clientProductPair.getKey() + ", " + clientProductPair.getValue());
            log.debug("Old total transaction amount: " + totalTransactionAmount.toString());
            log.debug("Adding new transaction amount: " + transactionAmount.toString());
            totalTransactionAmount = totalTransactionAmount.add(transactionAmount);
            log.debug("New total transaction amount: " + totalTransactionAmount.toString());
        }
        clientProductAmountMap.put(clientProductPair, totalTransactionAmount);
    }
}
