package com.abnamro.util.futuremovement;

import com.abnamro.util.futuremovement.model.InputSystem;

/**
 * A factory to create {@link InputFileFieldDefinitionLoader}s.
 */
public class InputFileFieldDefinitionLoaderFactory {

    /**
     * Constructs a {@link InputFileFieldDefinitionLoader} based on the input system
     * @param inputSystem the input system
     * @return a {@link InputFileFieldDefinitionLoader} for the specified {@link InputSystem}
     * @throws IllegalArgumentException if the specified input system is not implemented
     */
    public static InputFileFieldDefinitionLoader getInputFileFieldDefinitionLoader(InputSystem inputSystem) throws IllegalArgumentException {
        switch (inputSystem) {
            case SystemA:
                return new SystemAInputFileFieldDefinitionLoader();
            default:
                throw new IllegalArgumentException("Input system " + inputSystem + "not implemented.");
        }
    }

}
