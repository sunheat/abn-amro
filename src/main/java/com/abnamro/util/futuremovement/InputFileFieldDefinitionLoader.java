package com.abnamro.util.futuremovement;

import com.abnamro.util.futuremovement.model.InputFileField;
import com.abnamro.util.futuremovement.model.InputFileFieldDefinition;

import java.util.Map;

/**
 * Loads input file field definition
 */
public interface InputFileFieldDefinitionLoader {

    /**
     * Returns a map of fields and their definitions in {@link InputFileFieldDefinition} objects
     * @return {@link InputFileFieldDefinition} map
     */
    Map<InputFileField, InputFileFieldDefinition> loadInputFileFieldDefinition();

}
