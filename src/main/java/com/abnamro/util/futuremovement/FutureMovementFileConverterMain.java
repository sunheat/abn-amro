package com.abnamro.util.futuremovement;

import com.abnamro.util.futuremovement.model.InputSystem;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class FutureMovementFileConverterMain {

    private static final Logger log = LoggerFactory.getLogger(FutureMovementFileConverter.LOGGER_NAME);

    public static void main(String[] args) {
        log.info("Welcome to Future Movement Report Generator");

        if (!checkArgsLength(args)) {
            return;
        }

        InputSystem inputSystem = getInputSystemFromArg(args[0]);
        if (inputSystem == null) {
            log.error("Invalid input system" + args[0].trim());
            logValidInputSystems();
        }
        File inputFile = getInputFileFromArg(args[1]);
        if (inputFile == null) {
            return;
        }
        File outputFile = getOutputFileFromArg(args[2]);
        if (outputFile == null) {
            return;
        }

        printArgs(inputSystem, inputFile, outputFile);

        FutureMovementFileConverter converter = new FutureMovementFileConverterImpl();
        try {
            converter.convertFutureMovementFile(inputSystem, inputFile, outputFile);
        } catch (IOException e) {
            log.error("Unexpected I/O error: " + e.getMessage());
            log.error(ExceptionUtils.getMessage(e));
        }
    }

    private static void printArgs(InputSystem inputSystem, File inputFile, File outputFile) {
        log.info("Input system: " + inputSystem.getDisplayName());
        log.info("Input file: " + inputFile.getAbsolutePath());
        log.info("Output file: " + outputFile.getAbsolutePath());
    }

    private static InputSystem getInputSystemFromArg(String inputSystemArg) {
        return InputSystem.valueOf(inputSystemArg.trim());
    }

    private static boolean checkArgsLength(String[] args) {
        if (args.length != 3) {
            log.info("Usage: java -jar FutureMovementFileConverter.jar [InputSystem] [InputFilePath] [OutputFilePath]");
            log.info("Specify full, absolute paths of input and output files.");
            logValidInputSystems();
            return false;
        }
        return true;
    }

    private static File getInputFileFromArg(String inputFileArg) {
        File inputFile = new File(inputFileArg.trim());
        if (!inputFile.exists() || inputFile.isDirectory()) {
            log.error("Input file does not exist: " + inputFileArg.trim());
            return null;
        }
        return inputFile;
    }

    private static File getOutputFileFromArg(String outputFileArg) {
        if (outputFileArg.trim().length() == 0) {
            log.error("Please specify output file.");
            return null;
        }

        File outputFile = new File(outputFileArg.trim());
        if (outputFile.isDirectory()) {
            log.error("Output file must not be a directory.");
            return null;
        }
        if (outputFile.exists()) {
            log.warn("Output file " + outputFileArg.trim() + " already exists. It will be overwritten. Press ENTER to continue or enter any text and press ENTER to abort.");
            Scanner scanner = new Scanner(System.in);
            String input = scanner.nextLine();
            if (input.trim().length() == 0) {
                log.info("Output file will be overwritten.");
            } else {
                log.info("Aborting...");
                return null;
            }
        }

        return outputFile;
    }

    private static void logValidInputSystems() {
        log.info("Supported input systems are: ");
        for (InputSystem inputSystem : InputSystem.values()) {
            log.info("\t" + inputSystem.name());
        }
    }
}
