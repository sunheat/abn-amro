package com.abnamro.util.futuremovement;

import com.abnamro.util.futuremovement.model.InputFileField;
import com.abnamro.util.futuremovement.model.InputFileFieldDefinition;

import java.util.HashMap;
import java.util.Map;

import static com.abnamro.util.futuremovement.model.InputFileField.AccountNumber;
import static com.abnamro.util.futuremovement.model.InputFileField.BuySellCode;
import static com.abnamro.util.futuremovement.model.InputFileField.ClearingFeeCurCode;
import static com.abnamro.util.futuremovement.model.InputFileField.ClearingFeeDC;
import static com.abnamro.util.futuremovement.model.InputFileField.ClearingFeeDec;
import static com.abnamro.util.futuremovement.model.InputFileField.ClientNumber;
import static com.abnamro.util.futuremovement.model.InputFileField.ClientType;
import static com.abnamro.util.futuremovement.model.InputFileField.Commission;
import static com.abnamro.util.futuremovement.model.InputFileField.CommissionCurCode;
import static com.abnamro.util.futuremovement.model.InputFileField.CommissionDC;
import static com.abnamro.util.futuremovement.model.InputFileField.CurrencyCode;
import static com.abnamro.util.futuremovement.model.InputFileField.ExchBrokerFeeCurCode;
import static com.abnamro.util.futuremovement.model.InputFileField.ExchBrokerFeeDC;
import static com.abnamro.util.futuremovement.model.InputFileField.ExchBrokerFeeDec;
import static com.abnamro.util.futuremovement.model.InputFileField.ExchangeCode;
import static com.abnamro.util.futuremovement.model.InputFileField.ExpirationDate;
import static com.abnamro.util.futuremovement.model.InputFileField.ExternalNumber;
import static com.abnamro.util.futuremovement.model.InputFileField.Filler;
import static com.abnamro.util.futuremovement.model.InputFileField.FutureReference;
import static com.abnamro.util.futuremovement.model.InputFileField.MovementCode;
import static com.abnamro.util.futuremovement.model.InputFileField.OpenCloseCode;
import static com.abnamro.util.futuremovement.model.InputFileField.OppositePartyCode;
import static com.abnamro.util.futuremovement.model.InputFileField.OppositeTraderID;
import static com.abnamro.util.futuremovement.model.InputFileField.ProductGroupCode;
import static com.abnamro.util.futuremovement.model.InputFileField.QuantityLong;
import static com.abnamro.util.futuremovement.model.InputFileField.QuantityLongSign;
import static com.abnamro.util.futuremovement.model.InputFileField.QuantityShort;
import static com.abnamro.util.futuremovement.model.InputFileField.QuantityShortSign;
import static com.abnamro.util.futuremovement.model.InputFileField.RecordCode;
import static com.abnamro.util.futuremovement.model.InputFileField.SubaccountNumber;
import static com.abnamro.util.futuremovement.model.InputFileField.Symbol;
import static com.abnamro.util.futuremovement.model.InputFileField.TicketNumber;
import static com.abnamro.util.futuremovement.model.InputFileField.TraderInitials;
import static com.abnamro.util.futuremovement.model.InputFileField.TransactionDate;
import static com.abnamro.util.futuremovement.model.InputFileField.TransactionPriceDec;

/**
 * Implementation of {@link InputFileFieldDefinitionLoader} for System A input files.
 */
public class SystemAInputFileFieldDefinitionLoader implements InputFileFieldDefinitionLoader {

    public Map<InputFileField, InputFileFieldDefinition> loadInputFileFieldDefinition() {
        Map<InputFileField, InputFileFieldDefinition> definitionMap = new HashMap<>();
        definitionMap.put(RecordCode, new InputFileFieldDefinition(1, "RECORD CODE", 3, "315", 1));
        definitionMap.put(ClientType, new InputFileFieldDefinition(2, "CLIENT TYPE", 4, 4));
        definitionMap.put(ClientNumber, new InputFileFieldDefinition(3, "CLIENT NUMBER", 4, "(including 0 decimals)", 8));
        definitionMap.put(AccountNumber, new InputFileFieldDefinition(4, "ACCOUNT NUMBER", 4, "(including 0 decimals)", 12));
        definitionMap.put(SubaccountNumber, new InputFileFieldDefinition(5, "SUBACCOUNT NUMBER", 4, "(including 0 decimals)", 16));
        definitionMap.put(OppositePartyCode, new InputFileFieldDefinition(6, "ACCOUNT NUMBER", 6, 20));
        definitionMap.put(ProductGroupCode, new InputFileFieldDefinition(7, "PRODUCT GROUP CODE", 2, 26));
        definitionMap.put(ExchangeCode, new InputFileFieldDefinition(8, "ACCOUNT NUMBER", 4, 28));
        definitionMap.put(Symbol, new InputFileFieldDefinition(9, "SYMBOL", 6, 32));
        definitionMap.put(ExpirationDate, new InputFileFieldDefinition(11, "EXPIRATION DATE", 8, "(in format CCYYMMDD)", 38));
        definitionMap.put(CurrencyCode, new InputFileFieldDefinition(13, "CURRENCY CODE", 3, 46));
        definitionMap.put(MovementCode, new InputFileFieldDefinition(14, "MOVEMENT CODE", 2, 49));
        definitionMap.put(BuySellCode, new InputFileFieldDefinition(15, "BUY SELL CODE", 1, 51));
        definitionMap.put(QuantityLongSign, new InputFileFieldDefinition(68, "QUANTITY LONG SIGN", 1, 52));
        definitionMap.put(QuantityLong, new InputFileFieldDefinition(16, "QUANTITY LONG", 10, "(including 0 decimals)", 53));
        definitionMap.put(QuantityShortSign, new InputFileFieldDefinition(68, "QUANTITY SHORT SIGN", 1, 63));
        definitionMap.put(QuantityShort, new InputFileFieldDefinition(16, "QUANTITY SHORT", 10, "(including 0 decimals)", 64));
        definitionMap.put(ExchBrokerFeeDec, new InputFileFieldDefinition(17, "EXCH/BROKER FEE / DEC", 12, "(including 2 decimals)", 74));
        definitionMap.put(ExchBrokerFeeDC, new InputFileFieldDefinition(18, "EXCH/BROKER FEE D C", 1, 86));
        definitionMap.put(ExchBrokerFeeCurCode, new InputFileFieldDefinition(17, "EXCH/BROKER FEE CUR CODE", 3, 87));
        definitionMap.put(ClearingFeeDec, new InputFileFieldDefinition(19, "CLEARING FEE / DEC", 12, "(including 2 decimals)", 90));
        definitionMap.put(ClearingFeeDC, new InputFileFieldDefinition(18, "CLEARING FEE D C", 1, 102));
        definitionMap.put(ClearingFeeCurCode, new InputFileFieldDefinition(19, "ACCOUNT NUMBER", 3, 103));
        definitionMap.put(Commission, new InputFileFieldDefinition(86, "COMMISSION", 12, "(including 2 decimals)", 106));
        definitionMap.put(CommissionDC, new InputFileFieldDefinition(18, "COMMISSION D C", 1, 118));
        definitionMap.put(CommissionCurCode, new InputFileFieldDefinition(86, "COMMISSION CUR CODE", 3, 119));
        definitionMap.put(TransactionDate, new InputFileFieldDefinition(34, "TRANSACTION DATE", 8, "(in format CCYYMMDD)", 122));
        definitionMap.put(FutureReference, new InputFileFieldDefinition(36, "FUTURE REFERENCE", 6, "(including 0 decimals)", 130));
        definitionMap.put(TicketNumber, new InputFileFieldDefinition(37, "TICKET NUMBER", 6, 136));
        definitionMap.put(ExternalNumber, new InputFileFieldDefinition(38, "EXTERNAL NUMBER", 6, "(including 0 decimals)", 142));
        definitionMap.put(TransactionPriceDec, new InputFileFieldDefinition(20, "TRANSACTION PRICE / DEC", 15, "(including 7 decimals)", 148));
        definitionMap.put(TraderInitials, new InputFileFieldDefinition(66, "TRADER INITIALS", 6, 163));
        definitionMap.put(OppositeTraderID, new InputFileFieldDefinition(156, "OPPOSITE TRADER ID", 7, 169));
        definitionMap.put(OpenCloseCode, new InputFileFieldDefinition(65, "OPEN CLOSE CODE", 1, 176));
        definitionMap.put(Filler, new InputFileFieldDefinition(65, "FILLER", 127, 177));
        return definitionMap;
    }
}
