package com.abnamro.util.futuremovement.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * This class defines input fields of an input file
 */
public class InputFileFieldDefinition implements Serializable {

    protected int ref;
    protected String fieldName;
    protected int length;
    protected String description;
    protected int startPosition;

    /**
     * Construct a new <code>InputFileFieldDefinition</code> object with empty description
     */
    public InputFileFieldDefinition(int ref, String fieldName, int length, int startPosition) {
        this(ref, fieldName, length, "", startPosition);
    }

    public InputFileFieldDefinition(int ref, String fieldName, int length, String description, int startPosition) {
        this.ref = ref;
        this.fieldName = fieldName;
        this.length = length;
        this.description = description;
        this.startPosition = startPosition;
    }

    public int getRef() {
        return ref;
    }

    public String getFieldName() {
        return fieldName;
    }

    public int getLength() {
        return length;
    }

    public String getDescription() {
        return description;
    }

    public int getStartPosition() {
        return startPosition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        InputFileFieldDefinition that = (InputFileFieldDefinition) o;

        return new EqualsBuilder()
                .append(ref, that.ref)
                .append(length, that.length)
                .append(startPosition, that.startPosition)
                .append(fieldName, that.fieldName)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(ref)
                .append(fieldName)
                .append(length)
                .append(startPosition)
                .toHashCode();
    }
}
