package com.abnamro.util.futuremovement.model;

/**
 * Input fields for Future Movement report input files
 */
public enum InputFileField {
    RecordCode("Record code"), ClientType("Client Type"), ClientNumber("Client Number"),
    AccountNumber("Account Number"), SubaccountNumber("Subaccount Number"), OppositePartyCode("OppositePartyCode"),
    ProductGroupCode("Product Group Code"), ExchangeCode("Exchange Code"), Symbol("Symbol"),
    ExpirationDate("Expiration Date"), CurrencyCode("CurrencyCode"), MovementCode("MovementCode"),
    BuySellCode("Buy/Sell Code"), QuantityLongSign("Quantity Long Sign"), QuantityLong("Quantity Long"),
    QuantityShortSign("Quantity Short Sign"), QuantityShort("Quantity Short"), ExchBrokerFeeDec("Exchange/Broker Fee Decimals"),
    ExchBrokerFeeDC("Exchange/Broker Fee DC"), ExchBrokerFeeCurCode("Exchange/Broker Fee Currency Code"),
    ClearingFeeDec("Clearing Fee Decimals"), ClearingFeeDC("Clearing Fee DC"), ClearingFeeCurCode("Clearing Fee Currency Code"),
    Commission("Commission"), CommissionDC("Commission DC"), CommissionCurCode("Commission Currency Code"),
    TransactionDate("Transaction Date"), FutureReference("Future Reference"), TicketNumber("Ticket Number"),
    ExternalNumber("External Number"), TransactionPriceDec("Transaction Price Decimals"), TraderInitials("Trader Initials"),
    OppositeTraderID("Opposite Trader ID"), OpenCloseCode("Open/Close Code"), Filler("Filler");

    private String description;

    InputFileField(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
