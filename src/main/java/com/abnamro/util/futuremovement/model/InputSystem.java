package com.abnamro.util.futuremovement.model;

/**
 * Enumeration of supported input systems
 */
public enum InputSystem {

    SystemA("System A");

    private String displayName;

    /**
     * @param displayName a descriptive display name of the input system
     */
    InputSystem(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String toString() {
        return displayName;
    }
}
