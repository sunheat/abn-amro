package com.abnamro.util.futuremovement;

import com.abnamro.util.futuremovement.model.InputSystem;

import java.io.File;
import java.io.IOException;

/**
 * Converts input text file to future movement report CSV file.
 */
public interface FutureMovementFileConverter {

    static final String LOGGER_NAME = "FutureMovementFileConverter";

    /**
     * Converts input text file from the specified input system to output CSV report.
     *
     * @param inputSystem input system
     * @param inputFile   input file
     * @param outputFile  output file
     * @throws IOException if I/O errors
     */
    void convertFutureMovementFile(InputSystem inputSystem, File inputFile, File outputFile) throws IOException;

}
