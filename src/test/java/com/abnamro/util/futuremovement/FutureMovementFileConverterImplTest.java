package com.abnamro.util.futuremovement;

import com.abnamro.util.futuremovement.model.InputFileField;
import com.abnamro.util.futuremovement.model.InputFileFieldDefinition;
import com.abnamro.util.futuremovement.model.InputSystem;
import javafx.util.Pair;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class FutureMovementFileConverterImplTest {

    private static String LINE = "315CL  432100020001SGXDC FUSGX NK    20100910JPY01B 0000000001 0000000000000000000060DUSD000000000030DUSD000000000000DJPY201008200012380     688032000092500000000             O";

    private static Map<InputFileField, InputFileFieldDefinition> inputFileFieldDefinitionMap;

    private FutureMovementFileConverterImpl testee = new FutureMovementFileConverterImpl();

    @BeforeClass
    public static void setUp() {
        inputFileFieldDefinitionMap = InputFileFieldDefinitionLoaderFactory.getInputFileFieldDefinitionLoader(InputSystem.SystemA).loadInputFileFieldDefinition();
    }

    @Test
    public void testGetClientInformation() {
        String clientInfo = testee.getClientInformationString(LINE, inputFileFieldDefinitionMap);
        assertNotNull(clientInfo);
        assertTrue(clientInfo.length() > 0);
        assertEquals("CL  432100020001", clientInfo);
    }

    @Test
    public void testGetProductInformation() {
        String productInfo = testee.getProductInformation(LINE, inputFileFieldDefinitionMap);
        assertNotNull(productInfo);
        assertTrue(productInfo.length() > 0);
        assertEquals("SGX FUNK    20100910", productInfo);
    }

    @Test
    public void testTransactionAmount() {
        BigDecimal transactionAmount = testee.getTransactionAmount(LINE, inputFileFieldDefinitionMap);
        assertNotNull(transactionAmount);
        assertEquals(new BigDecimal(1), transactionAmount);
    }

    @Test
    public void testAddToTotalTransactionAmount() {
        Map<Pair<String, String>, BigDecimal> clientProductAmountMap = new HashMap<>();
        Pair<String, String> c1p1 = new Pair<>("c1", "p1");
        clientProductAmountMap.put(c1p1, new BigDecimal(3));
        Pair<String, String> c1p2 = new Pair<>("c1", "p2");
        // not putting into map yet
        BigDecimal c1p2Amount = new BigDecimal(5);
        Pair<String, String> c2p1 = new Pair<>("c2", "p1");
        BigDecimal c2p1Amount = new BigDecimal(7);
        clientProductAmountMap.put(c2p1, c2p1Amount);
        Pair<String, String> c2p2 = new Pair<>("c2", "p2");

        testee.addToTotalTransactionAmount(clientProductAmountMap, c1p1, new BigDecimal(11));
        assertEquals("Should add to existing amount for client 1, product 1", new BigDecimal(14), clientProductAmountMap.get(c1p1));
        testee.addToTotalTransactionAmount(clientProductAmountMap, c1p2, c1p2Amount);
        assertEquals("Should add initial amount for client 1, product 2", c1p2Amount, clientProductAmountMap.get(c1p2));
        assertEquals("Should not change amount for client 2, product 1", c2p1Amount, clientProductAmountMap.get(c2p1));
        assertNull("Should not see client-product record not added", clientProductAmountMap.get(c2p2));
    }

    @Test
    public void testConvertFutureMovementFile() throws IOException {
        File inputFile = new File(getClass().getClassLoader().getResource("input.txt").getFile());
        File outputFile = new File("output.csv");

        testee.convertFutureMovementFile(InputSystem.SystemA, inputFile, outputFile);
    }

}
