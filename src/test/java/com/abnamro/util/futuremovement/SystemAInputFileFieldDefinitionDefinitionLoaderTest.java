package com.abnamro.util.futuremovement;

import com.abnamro.util.futuremovement.model.InputFileField;
import com.abnamro.util.futuremovement.model.InputFileFieldDefinition;
import com.abnamro.util.futuremovement.model.InputSystem;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class SystemAInputFileFieldDefinitionDefinitionLoaderTest {

    private static InputFileFieldDefinitionLoader defLoader;

    @BeforeClass
    public static void setUp() {
        defLoader = InputFileFieldDefinitionLoaderFactory.getInputFileFieldDefinitionLoader(InputSystem.SystemA);
    }

    @Test
    public void testDefLoader() {
        assertNotNull("Factory should return InputFileFieldDefinitionLoader.", defLoader);
        assertEquals("Factory returned InputFileFieldDefinitionLoader should be of class SystemAInputFileFieldDefinitionLoader.", SystemAInputFileFieldDefinitionLoader.class, defLoader.getClass());
        Map<InputFileField, InputFileFieldDefinition> inputFileFieldDefinitions = defLoader.loadInputFileFieldDefinition();
        assertNotNull(inputFileFieldDefinitions);
        assertTrue("File input field definition list should not be empty.", inputFileFieldDefinitions.size() > 0);
    }

}
